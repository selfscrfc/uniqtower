package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var opts = getFlags()

type options struct {
	uniq      bool
	count     bool
	repeated  bool
	nrepeated bool
	fields    int
	chars     int
	reg       bool
	input     string
	output    string
}

func ignoreFC(str string) string {
	newStr := ""
	if strings.Count(str, " ")+1 > opts.fields {
		newStr = strings.Split(str, " ")[opts.fields]
		for _, v := range strings.Split(str, " ")[opts.fields+1:] {
			newStr += " " + v
		}
		if len(str) > 0 {
			newStr = newStr[opts.chars:]
			if opts.reg {
				newStr = strings.ToLower(newStr)
			}
		}
	}
	return newStr
}

func uniqFlag(data []string) []string {
	data = append(data, "EOF")
	var newData []string
	n := 0
	cnt := 1
	for i := 0; i < len(data); i++ {
		if data[i] == "EOF" {
			break
		}
		if ignoreFC(data[i]) == ignoreFC(data[i+1]) {
			if cnt == 1 {
				newData = append(newData, data[i])
				n++
			}
			cnt++
		} else {
			if cnt == 1 {
				newData = append(newData, data[i])
				n++
			}
			cnt = 1
		}
	}
	return newData
}

func uFlag(data []string) []string {
	data = append(data, "EOF")
	var newData []string
	n := 0
	cnt := 1
	for i := 0; i < len(data); i++ {
		if data[i] == "EOF" {
			break
		}
		if ignoreFC(data[i]) == ignoreFC(data[i+1]) {
			cnt++
		} else {
			if cnt == 1 {
				newData = append(newData, data[i])
				n++
			}
			cnt = 1
		}
	}
	return newData
}

func dFlag(data []string) []string {
	data = append(data, "EOF")
	var newData []string
	n := 0
	cnt := 1
	for i := 0; i < len(data); i++ {
		if data[i] == "EOF" {
			break
		}
		if ignoreFC(data[i]) == ignoreFC(data[i+1]) {
			cnt++
		} else {
			if cnt > 1 {
				newData = append(newData, data[i])
				n++
			}
			cnt = 1
		}
	}
	return newData
}

func cFlag(data []string) []string {
	data = append(data, "EOF")
	var newData []string
	n := 0
	cnt := 1
	for i := 0; i < len(data); i++ {
		if data[i] == "EOF" {
			break
		}
		if ignoreFC(data[i]) == ignoreFC(data[i+1]) {
			cnt++
		} else {
			newData = append(newData, strconv.Itoa(cnt)+" "+data[i])
			cnt = 1
			n++
		}
	}
	return newData
}
func saveData(data []string) {
	var f *os.File
	if opts.output == "" {
		f = os.Stdout
	} else {
		var e error
		f, e = os.Create(opts.output)
		check(e)
		defer f.Close()
	}

	for i := 0; i < len(data); i++ {
		_, e := f.WriteString(data[i] + "\n")
		check(e)
	}
}

func getData() []string {
	var f *os.File
	if opts.input == "" {
		f = os.Stdin
	} else {
		var e error
		f, e = os.Open(opts.input)
		check(e)
		defer f.Close()
	}
	var data []string
	scanner := bufio.NewScanner(f)

	for scanner.Scan() && scanner.Text() != "EOF" {
		check(scanner.Err())
		data = append(data, scanner.Text())
	}
	return data
}
func getFlags() options {
	opts := new(options)
	flag.BoolVar(&opts.count, "c", false, "bool")
	flag.BoolVar(&opts.repeated, "d", false, "bool")
	flag.BoolVar(&opts.nrepeated, "u", false, "bool")
	flag.IntVar(&opts.fields, "f", 0, "bool")
	flag.IntVar(&opts.chars, "s", 0, "bool")
	flag.BoolVar(&opts.reg, "i", false, "bool")

	flag.Parse()

	if opts.count && opts.repeated || opts.count && opts.nrepeated || opts.repeated && opts.nrepeated {
		fmt.Println("\nWrong flags! c, d, u can't be used simultaneously\n")
		os.Exit(1)
	}
	if (opts.count || opts.repeated || opts.nrepeated) == false {
		opts.uniq = true
	}

	tail := flag.Args()
	if len(tail) > 0 {
		opts.input = tail[0]
		if len(tail) > 1 {
			opts.output = tail[1]
		}
	}
	return *opts
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	data := getData()

	data = businessLogic(data)

	saveData(data)
}

func businessLogic(data []string) []string {
	switch true {
	case opts.uniq:
		data = uniqFlag(data)
	case opts.count:
		data = cFlag(data)
	case opts.repeated:
		data = dFlag(data)
	case opts.nrepeated:
		data = uFlag(data)
	}
	return data
}
